export const roundToTwoDecimals = (value: number): number => Math.round(value * 100.0) / 100.0;
