import { sendMsg } from './bot-api/bot';
import fetch from 'node-fetch';
import { readCounter, writeCounter } from './counter';
import {
  HSCoronaResult,
  getCasesByDates,
  getGrowthMultipliersBetweenDates,
  ConfirmedCase,
  DeathCase,
} from './corona-lib';
import { roundToTwoDecimals } from './utils';

const LOOP_INTERVAL = 30 * 1000;

const onlyUnique = (value: any, index: number, self: any[]): boolean => {
  return self.indexOf(value) === index;
};

const formConfirmedCasesText = (confirmedCases: ConfirmedCase[]) => {
  if (!confirmedCases.length) return `No new confirmed cases.`;

  const districts = confirmedCases
    .map(c => c.healthCareDistrict)
    .filter(onlyUnique)
    .join(', ');
  const sources = confirmedCases
    .map(c => c.infectionSourceCountry)
    .filter(onlyUnique)
    .join(', ');

  return (
    `New confirmed cases: ${confirmedCases.length}.\n` +
    `In healthcare district(s): ${districts}.\n` +
    `From source countries: ${sources}.`
  );
};

const formDeathCasesText = (deathCases: DeathCase[]) => {
  if (!deathCases.length) return 'No new deaths.';

  const districts = deathCases
    .map(c => c.healthCareDistrict)
    .filter(onlyUnique)
    .join(', ');

  return `New deaths: ${deathCases.length}.\nIn healthcare district(s): ${districts}.`;
};

// check HS.fi API every 10 seconds and post news if it has been updated
/* eslint-disable @typescript-eslint/explicit-function-return-type */
const mainLoop = async () => {
  try {
    const res = await fetch(
      'https://w3qa5ydb4l.execute-api.eu-west-1.amazonaws.com/prod/finnishCoronaData',
    );
    const json: HSCoronaResult = JSON.parse(res.body.read().toString());
    const newConfirmedCount = json.confirmed.length;
    const newDeathCount = json.deaths.length;
    const { confirmedCount: oldConfirmedCount, deathCount: oldDeathCount } = await readCounter();
    if (newConfirmedCount > oldConfirmedCount || newDeathCount > oldDeathCount) {
      const casesByDates = getCasesByDates(json.confirmed);
      const growthMultipliers = getGrowthMultipliersBetweenDates(casesByDates);
      const newConfirmedCases = json.confirmed.slice(0, newConfirmedCount - oldConfirmedCount);
      const newDeathCases = json.deaths.slice(0, newDeathCount - oldDeathCount);
      const mortalityRate = roundToTwoDecimals(newDeathCount / newConfirmedCount);
      const totalText =
        `Growth in the last 3 days: ${growthMultipliers[2]}, ${growthMultipliers[1]}, ${growthMultipliers[0]}.` +
        `\nTotal cases count: ${newConfirmedCount} confirmed, ${newDeathCount} deaths.` +
        `\nMortality rate: ${mortalityRate}.`;
      const confirmedText = formConfirmedCasesText(newConfirmedCases);
      const deathsText = formDeathCasesText(newDeathCases);

      const msg = `New data:\n\n${confirmedText}\n\n${deathsText}\n\n${totalText}`;

      await sendMsg(msg);
      await writeCounter(newConfirmedCount, newDeathCount);
    }
  } catch (err) {
    console.error(err);
  }
  setTimeout(mainLoop, LOOP_INTERVAL);
};

mainLoop();
