import { roundToTwoDecimals } from '../utils';

type Case = {
  id: string;
  date: string;
  healthCareDistrict: string;
};

export type ConfirmedCase = Case & {
  infectionSourceCountry: string;
  infectionSource: number | string;
};

export type DeathCase = Case;

export type HSCoronaResult = {
  confirmed: ConfirmedCase[];
  deaths: DeathCase[];
};

export type CasesByDate = {
  date: string;
  cases: Case[];
};

export const getCasesByDates = (cases: Case[]): CasesByDate[] => {
  return cases
    .sort((a, b) => (a.date > b.date ? -1 : 1))
    .reduce((acc: CasesByDate[], cur: Case) => {
      const date = cur.date.split('T')[0];
      if (acc.length == 0) {
        acc.push({ date, cases: [cur] });
      } else if (acc[acc.length - 1].date == date) {
        acc[acc.length - 1].cases.push(cur);
      } else if (acc[acc.length - 1].date !== date) {
        acc.push({ date, cases: [cur] });
      } else {
        console.error('UNEXPECTED CONDITION.');
      }
      return acc;
    }, []);
};

export const getGrowthMultipliersBetweenDates = (casesByDates: CasesByDate[]) =>
  casesByDates.map((val, index) =>
    index < casesByDates.length - 1
      ? roundToTwoDecimals(val.cases.length / casesByDates[index + 1].cases.length)
      : 1,
  );
